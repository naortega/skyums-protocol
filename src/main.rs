/*
 * Copyright (C) 2022 Ortega Froysa, Nicolás <nicolas@ortegas.org> All rights reserved.
 * Author: Ortega Froysa, Nicolás <nicolas@ortegas.org>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source
 *    distribution.
 */

extern crate rand;

use structopt::StructOpt;
use serde::{Serialize,Deserialize};
use serde_json::json;

// define the commandline parameters
#[derive(StructOpt)]
#[structopt(about = "An implementation of the Skyum's Protocol.")]
struct Opt
{
	#[structopt(short, long, default_value="100", help = "The number of inmates in the problem")]
	size:u32,
	#[structopt(long, help = "Print to stdout the JSON for the results")]
	json:bool,
}

// define the prisoner data
#[derive(Serialize, Deserialize)]
struct Prisoner
{
	// prisoner id number
	id:u32,
	// if the prisoner found his number
	found:bool,
	// number of attempt he found it in
	attempts:u32,
}

fn main()
{
	// read the commandline parameters
	let opts = Opt::from_args();

	// set number of prisoners in experiment
	let size = opts.size;

	// initialize the boxes with capacity for `size` number of boxes
	let mut boxes:Vec<u32> = Vec::with_capacity(size as usize);

	{
		// create vector of numbers to select randomly from
		let mut nums:Vec<u32> = (0..size).collect();

		while nums.len() > 0
		{
			// get a random index for nums
			let rand_i:usize = rand::random::<usize>() % nums.len();
			// add that element to the boxes vector
			boxes.push(nums[rand_i]);
			// remove the entry from the nums vector
			nums.remove(rand_i);
		}
	}

	// create the vector of prisoners
	let mut prisoners:Vec<Prisoner> = Vec::with_capacity(size as usize);

	// for every prisoner in the jail
	for i in 0..size
	{
		// create the prisoner for index `i`
		let mut prisoner_i = Prisoner {
			// assign the id number
			id: i,
			// assume the prisoner does not find his box
			found: false,
			// set number of attempts to 0
			attempts: 0,
		};
		// the next (first) box the prisoner is to open is the one with his number (`i`)
		let mut next_box:usize = i as usize;
		// while the prisoner still has attempts left AND he hasn't found his number
		while prisoner_i.attempts < size/2 && !prisoner_i.found
		{
			// count attempt
			prisoner_i.attempts += 1;
			// if the number inside the box is his number
			if boxes[next_box] == i
			{
				// assign `found` to true
				prisoner_i.found = true;
			}
			else
			{
				// if not found set the next_box to the number contained in the current box
				next_box = boxes[next_box] as usize;
			}
		}

		// add prisoner to list of prisoners in experiment
		prisoners.push(prisoner_i);
	}

	if !opts.json
	{
		// if any prisoner has not found their number
		if prisoners.iter().any(|i| !i.found)
		{
			println!("failure");
		}
		else
		{
			println!("success");
		}
	}
	else
	{
		// export data to JSON format
		let out_json = json!({
				"prisoners": prisoners
			});
		println!("{}", out_json);
	}
}
