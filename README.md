# Skyum's Protocol

This program is a reference to the problem in a [mathematical paper][1] and
referenced in a video by Veritasium titled ["The Riddle That Seems Impossible
Even If You Know The Answer"][2]. I don't feel like explaining the whole
problem, because I'd like to get coding, so just check out the video.

[1]: https://www.brics.dk/RS/03/44/BRICS-RS-03-44.pdf
[2]: https://odysee.com/@veritasium:f/the-riddle-that-seems-impossible-even-if:6?r=Cgf2A9Q55WHkdCBZhVdYdHg6bGCkmai8

## License

This code is licensed under the terms & conditions of the ZLib license (see the
[license file](LICENSE) for more information).
